public class Arreglo1{
   private double[] arreglo1;

   public Arreglo1(){
   arreglo1 = new double[10];
   }

//------------------------------------------------------------------------------------------------------------//

   public void setElementos(double elemento, int indice){
	   	   arreglo1[indice]=elemento;
	   }

		
//------------------------------------------------------------------------------------------------------------//
		
	public double calcuPromedio(){
		double promedio = 0, total=0;
		for (int indice=0;indice<arreglo1.length;indice++)
		{
			total+=arreglo1[indice];
		     promedio = total/arreglo1.length;
		}	
		return promedio;
		}
		
//------------------------------------------------------------------------------------------------------------//

	public  String mayorProm(){
		int valoresMayores=0;
			for (int indice=0;indice<arreglo1.length;indice++){
				if (arreglo1[indice]>calcuPromedio()){
					valoresMayores ++;
				}
				else{
					valoresMayores = 0;
				}//Fin del if, else
			}//Fin del for
			return "Los cantidad de valores mayores al promedio son "+valoresMayores;
	}
	
//------------------------------------------------------------------------------------------------------------//

	public  String menorProm(){
		String valoresMenores="";
			for (int indice=0;indice<arreglo1.length;indice++){
				if (arreglo1[indice]<calcuPromedio()){
					valoresMenores += " "+arreglo1[indice];
				}//Fin del if
			}//Fin del for
			return  "Los valores menores al promedio son: "+valoresMenores;
	}

//------------------------------------------------------------------------------------------------------------//

   public String toString(){
	   String elementos="Estos son los elementos del conjunto\n{";
	   for(int indice=0;indice<arreglo1.length;indice++)
	   {
		   elementos+=" "+arreglo1[indice];
		   }//Fin del for
	   return elementos+" }";
	   }
}//Fin de clase
