import javax.swing.JOptionPane;
public class MainArreglo2{
	public static void main (String arg[]){
     String datoLeido;
     Arreglo2 arreglo2 = new Arreglo2();
     int opcion=0;
     int elemento, indice;
     while (opcion!=8){
     datoLeido=JOptionPane.showInputDialog("--------------Escoja su opción--------------"
																											+"\n1. Llenar el arreglo aleatoriamente."
																											+"\n2. Mostrar los element0s del arreglo"
																											+"\n3. Mostrar un valor en el arreglo"
																											+"\n4. Mostrar posición de un valor en el arreglo"
																											+"\n5. Mostrar el valor del elemento mayor del arreglo"
																											+"\n6. Mostrar la posición del elemento menor"
																											+"\n7. Modificar arreglo"
																											+"\n8. Salir");
    opcion=Integer.parseInt(datoLeido);
	switch (opcion){
		case 1:
			JOptionPane.showMessageDialog(null,arreglo2.llenarArreglo());
		break;
	
		case 2:
			JOptionPane.showMessageDialog(null,arreglo2.toString());
		break;
		
		case 3:
			datoLeido=JOptionPane.showInputDialog("Digite el elemento que desea buscar en el arreglo");
				elemento=Integer.parseInt(datoLeido);
			JOptionPane.showMessageDialog(null,arreglo2.buscarElemento(elemento));
		break;


		case 4:
			datoLeido=JOptionPane.showInputDialog("Digite el elemento del que desea obtener la posicion");
				elemento=Integer.parseInt(datoLeido);
			JOptionPane.showMessageDialog(null,arreglo2.getPosicion(elemento));
		break;


		case 5:
			JOptionPane.showMessageDialog(null,arreglo2.elemMax());
		break;

		case 6:
			JOptionPane.showMessageDialog(null,arreglo2.elemMin());
		break;

		case 7:
			datoLeido=JOptionPane.showInputDialog("Digite el indice que desea modificar");
				indice=Integer.parseInt(datoLeido);
			datoLeido=JOptionPane.showInputDialog("Digite el elemento que desea asignarle al indice");
				elemento=Integer.parseInt(datoLeido);
			JOptionPane.showMessageDialog(null,arreglo2.modArreglo(indice, elemento));
		break;

		case 8:
			JOptionPane.showMessageDialog(null,"Gracias por usar este programa");
		break;
		default: JOptionPane.showMessageDialog(null,"Esa opción no está disponible");
		}
     
     }//Fin del while
}//Fin del main
}//Fin de MainArreglo1
