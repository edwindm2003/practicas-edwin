 public class ArregloABC{
   private int[] arregloA;
   private int[] arregloB;
   private int[] arregloC;
   private int elemento; 
		
		
	public String sumAlmArreglos(){
		arregloA = new int[45];
		for(int indice=0;indice<arregloA.length;indice++){ 
			 arregloA[indice] = (int)(Math.random()*50+1);
		}//Fin for arregloA
		
		arregloB = new int[45];
		for(int indice=0;indice<arregloB.length;indice++){ 
			  arregloB[indice] = (int)(Math.random()*50+1);
		}//Fin for arregloB
		
		arregloC = new int[45];
		for(int indice=0;indice<arregloC.length;indice++){ 
				 arregloC[indice] =  arregloA[indice] + arregloB[indice];
		}//Fin for arregloC
		return "Los arreglos se han sumado y almacenado correctamente";
	}
		

	   public String toString(){
		String elementosA = "";
			for(int indice=0;indice<arregloA.length;indice++){
				elementosA+=" "+arregloA[indice];
		   }//Fin del for
		   
		String elementosB = "";
			for(int indice=0;indice<arregloB.length;indice++){
				elementosB+=" "+arregloB[indice];
		   }//Fin del for
		   
		String elementosC = "";
			for(int indice=0;indice<arregloC.length;indice++){
				elementosC+=" "+arregloC[indice];
		   }//Fin del for

	   return "--------------------------------------------------------------------------- Elementos del arreglo A ----------------------------------------------------------------------------\n"+elementosA
					+"\n\n--------------------------------------------------------------------------- Elementos del arreglo B ---------------------------------------------------------------------------\n"+elementosB
					+"\n\n---------------------------------------------------- La suma de los elementos A y B almacenados en el arreglo C ----------------------------------------------------\n"+elementosC;
	   }
		
	
	
}//Fin de clase
	
