import javax.swing.JOptionPane;
public class MainArreglo1{
	public static void main (String arg[]){
     String datoLeido;
     Arreglo1 arreglo = new Arreglo1();
     int opcion=0;
     double elemento;
     while (opcion!=6){
     datoLeido=JOptionPane.showInputDialog("Escoja su opción\n"
																											+"\n1. Llenar el arreglo."
																											+"\n2.Calcular e imprimir el promedio de los valores almacenados en el arreglo."
																											+"\n3.Determinar cuántos valores son mayores que el promedio"
																											+"\n4.Determinar cuáles valores son menores que el promedio."
																											+"\n5.Mostrar los valores del arreglo"
																											+"\n6.Salir");
    opcion=Integer.parseInt(datoLeido);
	switch (opcion){
		case 1:
		for (int indice = 0;indice<10;indice++){
		    datoLeido = JOptionPane.showInputDialog("Digite el elemento  que desea asignarle al indice "+indice);
				elemento = Double.parseDouble(datoLeido);
		    arreglo.setElementos(elemento, indice);
		}
		break;
	
		case 2:
			JOptionPane.showMessageDialog(null,"El promedio de los valores del arreglo es "+arreglo.calcuPromedio());
		break;
		case 3:
			JOptionPane.showMessageDialog(null,arreglo.mayorProm());
		break;
		case 4:
			JOptionPane.showMessageDialog(null,arreglo.menorProm());
		break;
		case 5:
			JOptionPane.showMessageDialog(null,arreglo.toString());
		break;
		case 6:
			JOptionPane.showMessageDialog(null,"Gracias por usar este programa");
		break;
		default: JOptionPane.showMessageDialog(null,"Esa opción no está disponible");
		}
     
     }//Fin del while
}//Fin del main
}//Fin de MainArreglo1
