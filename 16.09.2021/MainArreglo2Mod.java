import javax.swing.JOptionPane;
public class MainArreglo2Mod{
	public static void main (String arg[]){
		String datoLeido;
		Arreglo2Mod arreglo2Mod = new Arreglo2Mod();
		int opcion=0;
		int elemento, indice, numIn, numFin;
		while (opcion!=12){
			datoLeido=JOptionPane.showInputDialog("\n-------------------------- Escoja su opción --------------------------\n"
																												+"\n1. Rellenar el arreglo aleatoriamente."
																												+"\n2. Mostrar los elementos del arreglo."
																												+"\n3. Buscar un valor en el arreglo."
																												+"\n4. Mostrar posición de un valor en el arreglo."
																												+"\n5. Mostrar el valor del elemento mayor del arreglo."
																												+"\n6. Mostrar la posición del elemento menor del arreglo."
																												+"\n7. Crear un rango en el arreglo."
																												+"\n8. Mostrar los elementos del rango del arreglo."
																												+"\n9. Mostrar posición de un valor en el rango del arreglo."
																												+"\n10. Cambiar posicion de maximo y minimo."
																												+"\n11. Modificar arreglo."
																												+"\n12. Salir.\n\n");
			opcion=Integer.parseInt(datoLeido);
			switch (opcion){
				case 1:
					JOptionPane.showMessageDialog(null,arreglo2Mod.llenarArreglo());
				break;
		
				case 2:
					JOptionPane.showMessageDialog(null,arreglo2Mod.toStringArre());
				break;

				case 3:
					datoLeido=JOptionPane.showInputDialog("Digite el elemento que desea buscar en el arreglo");
						elemento=Integer.parseInt(datoLeido);
					JOptionPane.showMessageDialog(null,arreglo2Mod.buscarElemento(elemento));
				break;
		
				case 4:
					datoLeido=JOptionPane.showInputDialog("Digite el elemento del que desea obtener la posicion");
						elemento=Integer.parseInt(datoLeido);
					JOptionPane.showMessageDialog(null,arreglo2Mod.getPosicion(elemento));
				break;
		
				case 5:
					JOptionPane.showMessageDialog(null,arreglo2Mod.elemMax());
				break;


				case 6:
					JOptionPane.showMessageDialog(null,arreglo2Mod.elemMin());
				break;

				case 7:
					datoLeido=JOptionPane.showInputDialog("Digite el numero desde donde desea  empezar el rango");
						numIn=Integer.parseInt(datoLeido);		
					datoLeido=JOptionPane.showInputDialog("Digite el numero de donde desea  terminar el rango");
						numFin=Integer.parseInt(datoLeido);
					JOptionPane.showMessageDialog(null,arreglo2Mod.rangoArre(numIn, numFin));
				break;

				case 8:
					JOptionPane.showMessageDialog(null,arreglo2Mod.toStringRang());
				break;

				case 9:
					datoLeido=JOptionPane.showInputDialog("Digite el elemento del que desea obtener la posicion en el rango del arreglo");
						elemento=Integer.parseInt(datoLeido);
					JOptionPane.showMessageDialog(null,arreglo2Mod.getPosicionRang(elemento));
				break;

				case 10:
					JOptionPane.showMessageDialog(null,arreglo2Mod.cambiarMaxMin());
				break;

				case 11:
					datoLeido=JOptionPane.showInputDialog("Digite el indice que desea modificar");
						indice=Integer.parseInt(datoLeido);
					datoLeido=JOptionPane.showInputDialog("Digite el elemento que desea asignarle al indice");
						elemento=Integer.parseInt(datoLeido);
					JOptionPane.showMessageDialog(null,arreglo2Mod.modArreglo(indice, elemento));
				break;
		
				case 12:
					JOptionPane.showMessageDialog(null,"Gracias por usar este programa");
				break;
				default: JOptionPane.showMessageDialog(null,"Esa opción no está disponible");
			}//Fin del Switch
		}//Fin del while
	}//Fin del main
}//Fin de MainArreglo2Mod
