public class Arreglo2Mod{
	private int[] arreglo2Mod;
	private int numero, elemento, max, min;
	private int indiceElementoMin;
	private int indiceElementoMax;
	private int numInicial = 0, numFinal = 0;
	private String elementos="Estos son los elementos del arreglo\n{";
	private boolean encontrado = false;
	
		public Arreglo2Mod(){
			arreglo2Mod = new int[20];
		}//------------------Fin de Constructor------------------//
		
    
		public String llenarArreglo(){ 
			for(int indice=0;indice<arreglo2Mod.length;indice++){ 
				do{
					encontrado = false;
					numero = (int)(Math.random()*50+1);
						for(int posicion=0;posicion<arreglo2Mod.length;posicion++){ 
							if(arreglo2Mod[posicion]==numero){
								encontrado = true;
							break;
							}
						}
				}
				while(encontrado);
				arreglo2Mod[indice] = numero;
			}
		return "El arreglo se lleno satisfactoriamente";
		}//------------------Fin de llenarArreglo------------------//


		public String rangoArre(int numIni, int numFin){
			numInicial = numIni;
			numFinal = numFin;
			String condicion = "";
			if(numInicial>=0 & numInicial<arreglo2Mod.length & numFinal>=0 & numFinal<arreglo2Mod.length ){
				for(int indice=numInicial;indice<=numFinal;indice++){ 
					elementos+=" "+arreglo2Mod[indice];
				}
				condicion = "Se creó el rango correctamente "+elementos+" }";
			}	
			else{
				condicion = "No se pudo crear el rango debido a que los indices ingresados no se encuentran";
			}
		return  condicion;
		}//------------------Fin de rangoArre------------------//
   
   
		public String toStringArre(){
			String elementos="Estos son los elementos del arreglo\n{";
			for(int indice=0;indice<arreglo2Mod.length;indice++){
				elementos+=" "+arreglo2Mod[indice];
			}//Fin del for
		return elementos+" }";
		}//------------------Fin de toStringArre------------------//
	   
	   
		public String toStringRang(){
			return elementos+" }";
		}//------------------Fin de toStringRang ------------------//
	   

		public String buscarElemento(int elemento){
			String condicion = "";
			for(int indice=0;indice<arreglo2Mod.length;indice++){
				if(elemento==arreglo2Mod[indice]){  
					condicion = "El elemento "+elemento+" sí se encuentra en el arreglo";
					indice=arreglo2Mod.length;
				}
				else{
					condicion = "El elemento "+elemento+" no se encuentra en el arreglo";
				}
			}//Fin del for
		return condicion;
		}//------------------Fin de buscarElemento ------------------// 
		  
			 
		public String getPosicion(int elemento){
			String condicion = "";
			int indiceElemento=-1;
			for(int indice=0;indice<arreglo2Mod.length;indice++){
				if(elemento==arreglo2Mod[indice]){
					indiceElemento = indice;
					indice=arreglo2Mod.length;
					condicion = "El elemento "+elemento+" se encuentra en la posición "+indiceElemento;
				}
				else{
					condicion =  "El elemento "+elemento+" no se encuentra en el arreglo";
				}
			}//Fin del for
		return condicion;
		}//------------------Fin de getPosicion------------------//
		

		public String getPosicionRang(int elemento){
			String condicion = "";
			int indiceElemento=-1;
			for(int indice=numInicial;indice<=numFinal;indice++){ 
				if(elemento==arreglo2Mod[indice]){
					indiceElemento = indice;
					indice=arreglo2Mod.length;
					condicion = "El elemento "+elemento+" se encuentra en la posición "+indiceElemento;
				}
				else{
					condicion =  "El elemento "+elemento+" no se encuentra en el rango";				
				}
			}//Fin del for
		   return condicion;
		}//------------------Fin del getPosicionRang------------------// 


		public String elemMax(){
		max=arreglo2Mod[0];
			for (int indice=1;indice<arreglo2Mod.length;indice++){
				if(max<=arreglo2Mod[indice]){
					max=arreglo2Mod[indice];
					indiceElementoMax = indice;
				}
			}
		return "El elemento mayor del arreglo es: "+max;
		}//------------------Fin de elemMax------------------//


		public String elemMin(){
			min = arreglo2Mod[0];
			for (int indice=1;indice<arreglo2Mod.length;indice++){
				if(min>=arreglo2Mod[indice]){
				min = arreglo2Mod[indice];
				indiceElementoMin = indice;
				}
			}
			return "El elemento minimo se encuentra en la posición "+indiceElementoMin;
		}//------------------Fin de elemMin------------------// 
		

		public String cambiarMaxMin(){
			arreglo2Mod[indiceElementoMin]=max;
			arreglo2Mod[indiceElementoMax]=min;
		return "El elemento maximo "+max+" ahora esta en el indice "+indiceElementoMin+" y el minimo "+min+" en el indice "+indiceElementoMax;
		}//------------------Fin de cambiarMaxMin------------------//
   

		public String modArreglo(int indice, int elemModi){ 
			arreglo2Mod[indice] = elemModi;
		return "Se modificó correctamente el valor";
		}//------------------Fin de modArreglo------------------//
}//Fin de clase Arreglo2
